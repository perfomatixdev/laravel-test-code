<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\County;
use App\City;
use App\ZipCode;
use Redirect;
use Validator;
class LocationController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        user_has_access(4, 'view');
        $countries = collect(Country::select('countries.country_id', 'country_name', 'partner_user_location_id')->orderBy('country_name')->leftJoin('partner_user_locations', 'partner_user_locations.country_id', '=', 'countries.country_id')->get());
        $countries = $countries->unique('country_id');
        $countries->values()->all();
        return view('location.index', compact('countries'))->with(['page_title' => 'Territories/Location', 'breadcrum' => 'log']);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        user_has_access(4, 'add');
        if ($request->dataType == "zipCode") {
            $rules = ['dataInput' => 'required'];
            $messages = ['required' => 'This field is required.', ];
            $this->validate($request, $rules, $messages);
        } else {
            $rules = ['dataInput' => 'required|max:255'];
            $messages = ['required' => 'This field is required.', 'max' => 'Only 255 Character length allowed!', ];
            $this->validate($request, $rules, $messages);
        }
        $input = $request->dataInput;
        $inputType = $request->dataType;
        $selectedParrentId = $request->selectedParrentId;
        switch ($inputType) {
            case 'country':
                $dataRow = new Country;
                $dataRow->country_name = $input;
                $dataRow->save();
            break;
            case 'state':
                $dataRow = new State;
                $dataRow->state_name = $input;
                $dataRow->country_id = $selectedParrentId;
                $dataRow->save();
            break;
            case 'county':
                $dataRow = new County;
                $dataRow->county_name = $input;
                $dataRow->state_id = $selectedParrentId;
                $dataRow->save();
            break;
            case 'city':
                $dataRow = new City;
                $dataRow->city_name = $input;
                $dataRow->county_id = $selectedParrentId;
                $dataRow->save();
            break;
            case 'zipCode':
                $dataRow = new ZipCode;
                $dataRow->zip_code = $input;
                $dataRow->city_id = $selectedParrentId;
                $dataRow->save();
            break;
        }
        echo json_encode($dataRow);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id) {
        user_has_access(4, 'view');
        $type = $request->type;
        $dataRow = "";
        switch ($type) {
            case 'country':
                $countries = collect(State::select('states.state_id', 'state_name', 'partner_user_location_id')->orderBy('state_name')->leftJoin('partner_user_locations', 'partner_user_locations.state_id', '=', 'states.state_id')->where('states.country_id', $id)->get());
                $dataRow = $countries->unique('state_id');
                $dataRow->values()->all();
            break;
            case 'state':
                $countries = collect(City::select('cities.city_id', 'cities.city_name', 'partner_user_location_id')->orderBy('city_name')->leftJoin('partner_user_locations', 'partner_user_locations.city_id', '=', 'cities.city_id')->where('cities.state_id', $id)->get());
                $dataRow = $countries->unique('city_id');
                $dataRow->values()->all();
            break;
            case 'city':
                $countries = collect(ZipCode::select('zip_codes.zip_id', 'zip_codes.zip_code', 'partner_user_location_id')->orderBy('zip_code')->leftJoin('partner_user_locations', 'partner_user_locations.zip_id', '=', 'zip_codes.zip_id')->where('zip_codes.city_id', $id)->get());
                $dataRow = $countries->unique('zip_id');
                $dataRow->values()->all();
            break;
        }
        if ($dataRow) {
            echo json_encode($dataRow);
        } else {
            echo json_encode("invalid");
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        user_has_access(4, 'delete');
        $type = $request->type;
        try {
            switch ($type) {
                case 'country':
                    $count = State::where('country_id', $id)->count();
                    if ($count == 0) {
                        $dataRow = Country::where('country_id', $id)->delete();
                    } else {
                        $dataRow = "invalid";
                    }
                break;
                case 'state':
                    $count = County::where('state_id', $id)->count();
                    if ($count == 0) {
                        $dataRow = State::where('state_id', $id)->delete();
                    } else {
                        $dataRow = "invalid";
                    }
                break;
                case 'county':
                    $count = City::where('county_id', $id)->count();
                    if ($count == 0) {
                        $dataRow = County::where('county_id', $id)->delete();
                    } else {
                        $dataRow = "invalid";
                    }
                break;
                case 'city':
                    $count = ZipCode::where('city_id', $id)->count();
                    if ($count == 0) {
                        $dataRow = City::where('city_id', $id)->delete();
                    } else {
                        $dataRow = "invalid";
                    }
                break;
                case 'zipCode':
                    $dataRow = ZipCode::where('zip_id', $id)->delete();
                break;
            }
        }
        catch(\Exception $e) {
            $dataRow = "invalid";
        }
        echo json_encode($dataRow);
    }
    /**
     * Search Location By name.
     *
     * @param  string  name
     * @return \Illuminate\Http\Response
     */
    public function locationSearch(Request $request) {
        user_has_access(4, 'view');
        $type = $request->type;
        $search_val = $request->searchValue;
        $parrentId = $request->parrentId;
        $dataRow = "";
        switch ($type) {
            case 'country':
                $dataRow = Country::select('*')->where('country_name', 'like', '%' . $search_val . '%')->orderBy('country_name')->get();
            break;
            case 'state':
                $dataRow = State::select('*')->where('country_id', $parrentId)->where('state_name', 'like', '%' . $search_val . '%')->orderBy('state_name')->get();
            break;
            case 'county':
                $dataRow = County::select('*')->where('state_id', $parrentId)->where('county_name', 'like', '%' . $search_val . '%')->orderBy('county_name')->get();
            break;
            case 'city':
                $dataRow = City::select('*')->where('county_id', $parrentId)->where('city_name', 'like', '%' . $search_val . '%')->orderBy('city_name')->get();
            break;
            case 'zipCode':
                $dataRow = ZipCode::select('*')->where('city_id', $parrentId)->where('zip_code', 'like', '%' . $search_val . '%')->orderBy('zip_code')->get();
            break;
        }
        if ($dataRow) {
            echo json_encode($dataRow);
        } else {
            echo json_encode("invalid");
        }
    }
    /**
     * Edit Location.
     *
     * @param  string  name
     * @return \Illuminate\Http\Response
     */
    public function editLocation(Request $request) {
        user_has_access(4, 'delete');
        $type = $request->type;
        $name = $request->newname;
        $id = $request->data_id;
        try {
            switch ($type) {
                case 'country':
                    $dataRow = Country::where('country_id', $id)->first();
                    $dataRow->country_name = $name;
                    $dataRow->save();
                break;
                case 'state':
                    $dataRow = State::where('state_id', $id)->first();
                    $dataRow->state_name = $name;
                    $dataRow->save();
                break;
                case 'county':
                    $dataRow = County::where('county_id', $id)->first();
                    $dataRow->county_name = $name;
                    $dataRow->save();
                break;
                case 'city':
                    $dataRow = City::where('city_id', $id)->first();
                    $dataRow->city_name = $name;
                    $dataRow->save();
                break;
                case 'zipCode':
                    $rules = ['newname' => 'numeric', ];
                    $messages = ['newname' => 'Enter Valid Zipocode!', ];
                    $validator = Validator::make($request->all(), $rules, $messages);
                    if ($validator->fails()) {
                        return "invaild";
                    }
                    $dataRow = ZipCode::where('zip_id', $id)->first();
                    $dataRow->zip_code = $name;
                    $dataRow->save();
                break;
            }
        }
        catch(\Exception $e) {
            $dataRow = "invalid";
        }
        echo json_encode($dataRow);
    }
}
