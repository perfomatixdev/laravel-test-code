<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailTemplate;
use Illuminate\Support\Str;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        user_has_access(2, 'view');
        $templates = EmailTemplate::where('is_deleted', 0)->get();
        return view('email.index')
            ->with(['page_title' => 'Email templates', 'templates' => $templates, 'user_access' => get_user_access(2) , 'breadcrum' => 'log', 'url_create' => '/emailTemplates/create', ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        user_has_access(2, 'add');
        return view('email.create')->with(['page_title' => 'Create template', 'breadcrum' => 'email']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        user_has_access(2, 'add');
        $rules = ['name' => 'required|max:30', 'body' => 'required', 'subject' => 'required|max:255', 'status' => 'required', 'template_type' => 'required',

        ];
        $messages = ['required' => 'The :attribute field is required.', 'max' => 'The :attribute field must be within :max character in length',

        ];
        $this->validate($request, $rules, $messages);
        $template_available = EmailTemplate::where('status', 1)->where('template_type', $request->template_type)
            ->where('is_deleted', 0)
            ->first();
        if (($request->status == 0) || (!$template_available))
        {
            $template = new EmailTemplate();
            $template->email_template_name = $request->name;
            $template->email_template_description = $request->description;
            $template->email_template_subject = $request->subject;
            $template->email_template_body = $request->body;
            $template->status = $request->status;
            $template->template_type = $request->template_type;
            $template->save();
            return redirect()
                ->route('emailTemplates.index')
                ->with(['status' => 'Templates has been created', 'user_access' => get_user_access(2) ]);
        }
        else
        {
            return back()
                ->withInput()
                ->with('status', 'This template type is already active. - Dummy Text');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        user_has_access(2, 'edit');
        $template = EmailTemplate::find($id);
        return view('email.edit')->with(['page_title' => 'Edit template', 'template' => $template, 'breadcrum' => 'email']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        user_has_access(2, 'edit');
        $rules = ['name' => 'required|max:30', 'body' => 'required', 'subject' => 'required|max:255',

        //'businessPhone' => 'max:25',
        'status' => 'required', 'template_type' => 'required',

        ];
        $messages = ['required' => 'The :attribute field is required.', 'max' => 'The :attribute field must be within :max character in length',

        ];
        $this->validate($request, $rules, $messages);
        $template_available = EmailTemplate::where('status', 1)->where('template_type', $request->template_type)
            ->where('email_template_id', '!=', $id)->first();
        if (($request->status == 0) || !($template_available))
        {
            $template = EmailTemplate::find($id);
            $template->email_template_name = $request->name;
            $template->email_template_description = $request->description;
            $template->email_template_subject = $request->subject;
            $template->email_template_body = $request->body;
            $template->status = $request->status;
            $template->template_type = $request->template_type;
            $template->save();
            return redirect()
                ->route('emailTemplates.index')
                ->with(['status' => 'Template has been updated', 'user_access' => get_user_access(2) ]);
        }
        else
        {
            return back()
                ->withInput()
                ->with('status', 'This template type is already active - Dummy Text.');
        }

    }

    /**
     * Change the status of an email template.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function udpateStatus(Request $request)
    {
        user_has_access(2, 'edit');
        $template = EmailTemplate::find($request->template_id);
        if ($request->status == 1)
        {
            EmailTemplate::where('template_type', $template->template_type)
                ->update(['status' => 0]);
        }
        $template_type = str_replace('_', ' ', $template->template_type);
        $template->status = $request->status;
        $template->save();
        if ($request->status == 1) $request->session()
            ->flash('status', 'The active email template with type ' . Str::title($template_type) . ' changed');

        return response()->json(array(
            'message' => 'Updated '
        ) , 200);
    }
    /**
     * Destroy an email template.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function emailDelete(Request $request)
    {
        user_has_access(2, 'delete');
        $emailid = $request->emailid;
        $email = EmailTemplate::find($emailid);
        $email->is_deleted = 1;
        $email->save();
        echo json_encode("Status updated");
    }

}

