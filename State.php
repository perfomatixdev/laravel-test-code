<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {
    /**
     * The "name" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $primaryKey = 'state_id';
    /**
     * Indicates if the timestamps are true or false.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['state_id', 'state_name', 'country_id'];
    /**
     * Get the cities for the state.
     */
    public function cities() {
        return $this->hasMany('App\City', 'state_id')->orderBy('city_name');
    }
}